import React from 'react';
import Row from '../row/row';

const Board = (props) =>  {
  const cells = props.cells || [];
  return <div>
         { cells
            .map((row, index) => <Row 
                                  handleCellClick={props.handleCellClick} 
                                  key={index} 
                                  row={row} />) }
          </div>;
}

export default Board;

import React from 'react';
import { shallow, simulate } from 'enzyme'
import Board from './board';
import Row from '../row/row';

it('renders multiple rows when given 2d cells array', () => {
  const cells = [[{id: 1, alive: false}, {id: 2, alive: true}], [{id: 3, alive: false}, {id: 4, alive: true}]];
  const wrapper = shallow(<Board cells={cells} />);
  expect(wrapper.find(Row).length).toEqual(2);
});

it('does not render rows when no array is given', () => {
  const wrapper = shallow(<Board/>);
  expect(wrapper.find(Row).length).toEqual(0);
});

export class EventListener {
  constructor() {
    this.services = new Map();
  }

  register(service, action, callback) {
    if (this.services.has(service)) {
      this.services.get(service)[action].push(callback);
    } else {
      this.services.set(service, {[action]: [callback]});
    }
  }

  publish(service, action) {
    const callbacks = this.services.has(service) && this.services.get(service)[action];
    if (callbacks){
      for (const fn of callbacks) {
        fn();
      }
    }
  }
}

export default new EventListener();
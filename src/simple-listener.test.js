import { EventListener } from './simple-listener';

describe('Simple Listener', () => {
  let listener, service, action, callback;
  beforeEach(() => {
    listener = new EventListener();
    service = jest.fn();
    action = 'update';
    callback = jest.fn();
  });

  afterEach(() => {
    service.mockClear();
    callback.mockClear();
  });
  it('should create a services map on initialization', () => {
    expect(listener.services instanceof Map).toBe(true);
  });

  it('should register action-name and callback for a service in an array', () => {   
    listener.register(service, action, callback);
    expect(listener.services.get(service)[action]).toContain(callback);   
  });

  it('should add to that arrary if the service and action are already registered', () => {
    const secondCb = jest.fn();
    listener.register(service, action, callback);
    listener.register(service, action, secondCb);
    expect(listener.services.get(service)[action]).toContain(callback);  
    expect(listener.services.get(service)[action]).toContain(secondCb);  
    secondCb.mockClear();
  });

  it('should execute a callback if appropriate action on service is published', () => {
    listener.register(service, action, callback);
    listener.publish(service, action);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  it('should not execute callback if wrong action is published', () => {
    listener.register(service, action, callback);
    listener.publish(service, 'saved');
    expect(callback).toHaveBeenCalledTimes(0);
  });
});
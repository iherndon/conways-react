import React from 'react';
import Cell from '../cell/cell';
import './row.css';

const Row = (props) =>  {
  const row = props.row || [];
  return  <div className="row"> 
            { row.map(cell => <Cell onClick={props.handleCellClick} key={cell.id} cell={cell}/>) } 
          </div>;
}

export default Row;

import React from 'react';
import { shallow } from 'enzyme'
import Row from './row';
import Cell from '../cell/cell';

it('renders multiple cells when given a row prop', () => {
  const row = [{id: 1, alive: false}, {id: 2, alive: true}];
  const wrapper = shallow(<Row row={row} />);
  expect(wrapper.find(Cell).length).toEqual(2);
});

it('renders no cells when not given a row prop', () => {
  const wrapper = shallow(<Row />);
  expect(wrapper.find(Cell).length).toEqual(0);
});
import React from 'react';
import { shallow } from 'enzyme'
import GameRules from './game-rules';

it('renders with rules as list-itmes from props.rules', () => {
  const wrapper = shallow(<GameRules rules={['something', 'else']}/>);
  const items = wrapper.find('li');
  expect(items.length).toEqual(2);
});

it('does not renders with rules as list-itmes if no props.rules', () => {
  const wrapper = shallow(<GameRules />);
  const items = wrapper.find('li');
  expect(items.length).toEqual(0);
});



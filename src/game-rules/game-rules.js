import React from 'react';
import './game-rules.css';

const GameRules = (props) =>  {
  const rules = props.rules || [];
  return <div>  
          <h3>Rules</h3>
            <ul>
              { rules.map((rule, index) => <li key={index}>{rule}</li>) }
            </ul>
         </div>;
}

export default GameRules;

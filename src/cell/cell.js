import React, { Component } from 'react';
import classnames from 'classnames';
import './cell.css';

class Cell extends Component {
  constructor(props){
    super(props)
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick(this.props.cell);
  }

  render(){
    const cellClass = classnames('cell', {'alive': this.props.cell.alive})
    return (
         <div onClick={this.handleClick} className={cellClass}></div>
    );
  }
} 

export default Cell;

import React from 'react';
import { shallow, simulate } from 'enzyme';
import ReactDOM from 'react-dom';
import Cell from './cell';

it('renders cell with correct classname', () => {
  const cell = shallow(<Cell cell={{alive: true}} />)
  expect(cell.hasClass('cell')).toBe(true);
  expect(cell.hasClass('alive')).toBe(true);
});

it('calls onClick when clicked', () => {
  const onClick = jest.fn();
  const cell = shallow(<Cell cell={{alive: true}} onClick={onClick} />);
  cell.simulate('click');
  expect(onClick).toHaveBeenCalledTimes(1);
});
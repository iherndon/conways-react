import Game from './game';
import { gameBoards } from '../constants';
import listener from '../simple-listener';
listener.publish = jest.fn();

const mockNeighbors3 = [
  {alive:true},
  {alive:false},
  {alive:false},
  {alive:true},
  {alive:false},
  {alive:false},
  {alive:true},
  {alive:false}
]

const mockNeighbors1 = [
  {alive:false},
  {alive:false},
  {alive:false},
  {alive:false},
  {alive:false},
  {alive:false},
  {alive:true},
  {alive:false}
]

const mockNeighbors5 = [
  {alive:true},
  {alive:true},
  {alive:true},
  {alive:true},
  {alive:false},
  {alive:false},
  {alive:true},
  {alive:false}
]


describe('Game Service', () => {
  let game;
  beforeEach(() => {
    listener.publish.mockClear();
    game = new Game({tickInterval: 125, rowCount: 40});
  });
  describe('object construction', () => {

    it('should create tickInterval, rowCount, and cell count with appropriate values', () => {
      expect(game.rowCount).toBe(40);
      expect(game.cellCount).toBe(40);
      expect(game.tickInterval).toBe(125);
    });
    
    it('should create a gameboard on initialization ', () => {
      expect(Array.isArray(game.board)).toBe(true);
      expect(game.board.length).toBeGreaterThan(0);
    });
  });

  describe('methods', () => {
    describe('static getOptions', () => {
      it('should return an array of objects with the keys from gameBoards constant', () => {
        expect(Game.getOptions()[0].value).toEqual(Object.keys(gameBoards)[0]);
      });
    });

    describe('getBoard', () => {
      it('should return the board', () => {
        expect(game.getBoard()).toEqual(game.board);
      });
    });

    describe('isBoardEmpty', () => {
      it('should return true if there are no alive cells', () => {
        expect(game.isBoardEmpty()).toBe(true);
      });

      it ('should return false if there are any alive cells', () => {
        game.board[0][0].alive = true;
        expect(game.isBoardEmpty()).toBe(false);
      });
    });

    describe('setBoard', () => {
      beforeEach(()=> {
        game.setBoard('perpendicular');
      });
      it('should should the board based on gameBoards constant', () => {
        const perpendicular = gameBoards.perpendicular[0];
        expect(game.board[perpendicular[0]][perpendicular[1]].alive).toBe(true)
      });

      it('should publish that the board has been updated', () => {
        expect(listener.publish).toHaveBeenCalledTimes(1);
        expect(listener.publish).toHaveBeenCalledWith(game, 'board-updated');
      });
    });

    describe('clearBoard', () => {
      beforeEach(() => {
        game.clearBoard();
      });
      it('should set alive -> false on all cels',  () => {
        const flattened = [].concat.apply([], game.board);
        const unpopulatedCells = flattened.filter(cell => !cell.alive);
        expect(flattened.length).toEqual(unpopulatedCells.length);
      });

      it('should publish that the board has been updated', () => {
        expect(listener.publish).toHaveBeenCalledTimes(1);
        expect(listener.publish).toHaveBeenCalledWith(game, 'board-updated');
      });
    });

    describe('_createBoard', () => {
      it('should create a 2d array with the same number of rows and columns', () => {
        expect(game.board.length).toEqual(game.board[0].length);
      });
    });

    describe('getCellStateForNextTick', () => {     
      it('should call .getNeighbors', () => {
        game.getNeighbors = jest.fn(() => mockNeighbors3);
        game.getCellStateForNextTick({alive: false});
        expect(game.getNeighbors).toHaveBeenCalledTimes(1);
        game.getNeighbors.mockClear();
      });

      it('should return a live cell if the cell starts off dead and there are three live neighbors', () => {
        game.getNeighbors = jest.fn(() => mockNeighbors3);
        expect(game.getCellStateForNextTick({alive: false}).alive).toBe(true);
        game.getNeighbors.mockClear();
      });

      it('should return dead cell if the cell starts off alive and there are less than two neighbors alive', () => {
        game.getNeighbors = jest.fn(() => mockNeighbors1);
        expect(game.getCellStateForNextTick({alive: true}).alive).toBe(false);
        game.getNeighbors.mockClear();
      });

      it('should return dead cell if the cell starts off alive and there are more than three neighbors alive', () => {
        game.getNeighbors = jest.fn(() => mockNeighbors5);
        expect(game.getCellStateForNextTick({alive: true}).alive).toBe(false);
        game.getNeighbors.mockClear();
      });

      it('should return the same cell state in every other case', () => {
        game.getNeighbors = jest.fn(() => mockNeighbors5);
        expect(game.getCellStateForNextTick({alive: false}).alive).toBe(false);
        game.getNeighbors.mockClear();

        game.getNeighbors = jest.fn(() => mockNeighbors3);
        expect(game.getCellStateForNextTick({alive: true}).alive).toBe(true);
        game.getNeighbors.mockClear();
      });
    });

    describe('getNeighbors', () => {
      it('should provide an array of up to 8 neighbors of the current cell', () => {
        const result = game.getNeighbors([20, 20]);
        const positions = result.map(cell => cell.position);
        expect(positions).toContainEqual([19, 19]);
        expect(positions).toContainEqual([19, 20]);
        expect(positions).toContainEqual([19, 21]);
        expect(positions).toContainEqual([20, 19]);
        expect(positions).toContainEqual([20, 21]);
        expect(positions).toContainEqual([21, 19]);
        expect(positions).toContainEqual([21, 20]);
        expect(positions).toContainEqual([21, 21]);
      });
    });

    describe('nextTick', () => {
      it('should call .getCellStateForNextTick', () => {
        game.getCellStateForNextTick = jest.fn(() => {alive: true});
        game.nextTick();
        expect(game.getCellStateForNextTick).toHaveBeenCalled;
        game.getCellStateForNextTick.mockClear();
      });

      it('should publish board update', () => {
        game.nextTick();
        expect(listener.publish).toHaveBeenCalledTimes(1);
        expect(listener.publish).toHaveBeenCalledWith(game, 'board-updated');
      });
    });

    describe('updateCell', () => {
      it('should change the alive value for the cell', () => {
        game.updateCell({alive: true, position: [20,20]});
        expect(game.board[20][20].alive).toBe(false);
      });

      it('should publish board update', () => {
        game.updateCell({alive: false, position: [20,20]});
        expect(listener.publish).toHaveBeenCalledTimes(1);
        expect(listener.publish).toHaveBeenCalledWith(game, 'board-updated');
      });
    });

  });
});
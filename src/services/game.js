import { gameBoards } from '../constants';
import listener from '../simple-listener';

class Game {
  constructor(options) {
    this.rowCount = options.rowCount;
    this.cellCount = options.rowCount;
    this.tickInterval = options.tickInterval;
    this.board = this._createBoard();
  }

  static getOptions() {
    return Object.keys(gameBoards).map(options => ({value: options}));
  }

  getBoard() {
    return this.board;
  }

  isBoardEmpty() {
    const flattened = [].concat.apply([], this.board);
    return flattened.length === flattened.filter(cell => !cell.alive).length;
  }

  setBoard(type) {
    const coordinates = gameBoards[type];
    const board = this.board.map(row => row.map(cell => Object.assign(cell, {alive: false})));
    for (const position of coordinates) {
      board[position[0]][position[1]].alive = true;
    }
    this.board = board;
    listener.publish(this, 'board-updated');
  }

  clearBoard() {
    const board = this.board.map(row => row.map(cell => Object.assign(cell, {alive: false})));
    this.board = board;
    listener.publish(this, 'board-updated');
  }

  updateCell(cell) {
    const board = [].concat(this.board);
    board[cell.position[0]][cell.position[1]].alive = !cell.alive;
    this.board = board;
    listener.publish(this, 'board-updated');
  }
  
  tick() {
    this.intervalId = setInterval(() => {
      this.nextTick()
    }, this.tickInterval);
  }
  
  stopTick() {
    clearInterval(this.intervalId);
  }
  
  _createBoard() {
    const unpopulatedCell = { alive: false };
    const result = [];
    let id = 0;
    for (let i = 0; i < this.rowCount; i++) {
      const row = [];
      for (let j = 0; j < this.cellCount; j++) {
        const position = [i, j];
        row[j] = Object.assign({}, unpopulatedCell, { id, position });
        id++;
      }
      result[i] = row;
    }
    return result;
  }
  nextTick() {
    const board = this.board.map((row) => {
      return row.map((cell) => {
        return this.getCellStateForNextTick(cell);
      });
    });
    this.board = board;
    listener.publish(this, 'board-updated');
  }

  getCellStateForNextTick(cell) {
    const neighbors = this.getNeighbors(cell.position);
    const liveNeighbors = neighbors.filter(cell => cell && cell.alive);
    if (cell.alive && (liveNeighbors.length < 2 ||  liveNeighbors.length > 3)) {
      return Object.assign({}, cell, { alive: false });
    } 
    if (!cell.alive && liveNeighbors.length === 3) {
      return Object.assign({}, cell, { alive: true });
    }
    return cell;
  }

  getNeighbors(position) {
    const row = position[0];
    const column = position[1];
    const rowMinus = row - 1;
    const rowPlus = row + 1;

    return [
      this.board[rowMinus] && this.board[rowMinus][column - 1],
      this.board[rowMinus] && this.board[rowMinus][column], 
      this.board[rowMinus] && this.board[rowMinus][column + 1],
      this.board[row][column - 1],
      this.board[row][column + 1], 
      this.board[rowPlus] &&  this.board[rowPlus][column - 1],
      this.board[rowPlus] && this.board[rowPlus][column],
      this.board[rowPlus] && this.board[rowPlus][column + 1],
    ];
  }
}

export default Game;
import React from 'react';


const Select = (props) =>  {
  return <select disabled={props.isRunning} value={props.value} onChange={props.onChange}>
          <option disabled value=''>choose starting position</option>
          { props.options.map((opt, index) => <option key={index}> {opt.value} 
          </option>) }
        </select>
}

export default Select;

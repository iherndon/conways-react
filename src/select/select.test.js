import React from 'react';
import { shallow, simulate } from 'enzyme'
import Select from './select';

const options = [{value: 'something'}, {value:'else'}];

it('renders with options totaling length of props.options + 1', () => {
  const wrapper = shallow(<Select options={options}/>);
  expect(wrapper.children().length).toEqual(3);
});

it('is disabled if the game is running', () => {
  const wrapper = shallow(<Select isRunning={true} options={options}/>);
  expect(wrapper.prop('disabled')).toBe(true);
});

it('is not disabled if the game is not running', () => {
  const wrapper = shallow(<Select isRunning={false} options={options}/>);
  expect(wrapper.prop('disabled')).toBe(false);
});

it('should call props.onChange if change event occurs', () => {
  const onChange = jest.fn();
  const wrapper = shallow(<Select onChange={onChange} options={options}/>);
  wrapper.simulate('change');
  expect(onChange).toHaveBeenCalledTimes(1);
});


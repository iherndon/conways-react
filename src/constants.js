export const gameBoards = {
  beacon: [[9,17], [9,18], [10,17], [11,20], [12,20], [12,19]],
  perpendicular: [[17,19], [18,19], [19,19], [20,19], [20,18], [20,20]],
  'mirror perpendicular': [[17,19], [18,19], [19,19], [20,19], [20,18], [20,20], [14, 19], [13, 19], [12, 19], [11, 19], [11,18], [11, 20]],
}

export const rules = [
  'Any live cell with fewer than two live neighbors dies, as if caused by under-population',
  'Any live cell with two or three live neighbors lives on to the next generation',
  'Any live cell with more than three live neighbors dies, as if by overcrowding',
  'Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction'
];
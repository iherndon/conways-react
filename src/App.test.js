import React from 'react';
import { shallow, simulate } from 'enzyme'
import App from './App';
import GameRules from './game-rules/game-rules';
import Board from './board/board';
import Select from './select/select';

it('renders with one Board', () => {
  const app = shallow(<App />);
  expect(app.find(Board).length).toEqual(1);
});

it('renders with one Select', () => {
  const app = shallow(<App />);
  expect(app.find(Select).length).toEqual(1);
});

it('renders with one GameRules', () => {
  const app = shallow(<App />);
  expect(app.find(GameRules).length).toEqual(1);
});

it('renders with 3 buttons', () => {
  const app = shallow(<App />);
  expect(app.find('button').length).toEqual(3);
})

it('should have start and clear buttons disabled if game is running', () => {
  const app = shallow(<App />);
  app.instance().setState({isRunning: true});
  app.update();
  expect(app.find('.start').prop('disabled')).toBe(true);
  expect(app.find('.clear').prop('disabled')).toBe(true);
});

it('should getBoard from the game service when updating board', () => {
  const app = shallow(<App />);
  const gameService = app.instance().state.game;
  gameService.getBoard = jest.fn();
  app.instance().updateBoard();
  expect(gameService.getBoard).toHaveBeenCalledTimes(1)
});

it('should call tick from game service when .start is executed', () => {
  const app = shallow(<App />);
  const gameService = app.instance().state.game;
  app.instance().setState = jest.fn();
  gameService.tick = jest.fn();
  app.instance().start();
  expect(gameService.tick).toHaveBeenCalledTimes(1);
  expect(app.instance().setState).toHaveBeenCalledWith({isRunning: true});
});

it('should call clearBoard from game service when .clear is executed', () => {
  const app = shallow(<App />);
  const gameService = app.instance().state.game;
  gameService.clearBoard = jest.fn();
  app.instance().setState = jest.fn()
  app.instance().clear();
  expect(gameService.clearBoard).toHaveBeenCalledTimes(1);
  expect(app.instance().setState).toHaveBeenCalledWith({selectValue: ''});
});

it('should call stopTick from game service when .stop is executed', () => {
  const app = shallow(<App />);
  const gameService = app.instance().state.game;
  gameService.stopTick = jest.fn();
  app.instance().setState = jest.fn()
  app.instance().stop();
  expect(gameService.stopTick).toHaveBeenCalledTimes(1);
  expect(app.instance().setState).toHaveBeenCalledWith({isRunning: false});
});

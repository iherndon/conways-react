import React, { Component } from 'react';
import Board from './board/board';
import Select from './select/select';
import GameRules from './game-rules/game-rules';
import Game from './services/game';
import listener from './simple-listener';
import { rules } from './constants';

import './App.css';

class App extends Component {
  constructor (props) {
    super(props);
    const game = new Game({tickInterval: 125, rowCount: 40});
    this.state = {
      options: Game.getOptions(),
      cells: game.getBoard(), 
      selectValue: '',
      isRunning: false,
      rules,
      game,
    };

    this.start = this.start.bind(this);
    this.stop = this.stop.bind(this);
    this.clear = this.clear.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCellClick = this.handleCellClick.bind(this);
    this.updateBoard = this.updateBoard.bind(this);

    listener.register(game, 'board-updated', this.updateBoard);
  }

  updateBoard() {
    const cells = this.state.game.getBoard();
    this.setState({ cells });
  }

  handleCellClick(cell) {
    if(!this.state.isRunning) {
      this.state.game.updateCell(cell);
      const cells =  this.state.game.getBoard();
      this.setState({ cells });
    }
  }

  start() {
    this.state.game.tick();
    this.setState({ isRunning: true });
  }

  stop() {
    this.state.game.stopTick();
    this.setState({ isRunning: false });
  }

  clear() {
    this.state.game.clearBoard();
    this.setState({selectValue: ''});
  }

  handleChange (evt) {
    const selectValue = evt.target.value
    this.state.game.setBoard(selectValue);
    const cells = this.state.game.getBoard();
    this.setState({cells, selectValue});
  }

  render() {
    return (
      <div className="App">
        <div>
          <Board handleCellClick={this.handleCellClick} cells={this.state.cells} />
        </div>
        <div className="right-panel">
          <h1>Conway's Game of Life in React</h1>
          <GameRules rules={this.state.rules} />
          <h4>You can click on cells to create your own board or enhance an existing one</h4>
          <Select isRunning={this.state.isRunning} value={this.state.selectValue} options={this.state.options} onChange={this.handleChange} />
          <button 
            className="start" 
            disabled={this.state.isRunning || this.state.game.isBoardEmpty()} 
            onClick={this.start}>Start</button>
          <button 
          className="stop" 
          disabled={!this.state.isRunning || this.state.game.isBoardEmpty()} 
          onClick={this.stop}>Stop</button>
          <button 
          className="clear" 
          disabled={this.state.isRunning || this.state.game.isBoardEmpty()} 
          onClick={this.clear}>Clear</button>
        </div>
      </div>
    );
  }
}

export default App;

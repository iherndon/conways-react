# Conway's Game of Life with React

## Get Started

### Setup

To get started clone the repo and install node modules using the commands below:

`cd path/to/repo`

`npm install`

### Running the app locally

To run the app locally, make sure you are in the correct directory and use the command:

`npm start`

The app should open for you on `http://localhost:3000/`

### Running tests

To run tests use the command:

`npm test`

To run test with coverage use the command:

`npm run test:coverage`

